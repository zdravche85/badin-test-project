
## Badin Soft Test Project

### Instructions for setuping the project


Please follow commands in order they are written:
- sudo git clone https://gitlab.com/zdravche85/badin-test-project.git 
- sudo chmod 777 badin-test-project/ -R
- sudo chmod www-data badin-test-project/ -R
- composer install
- sudo cp .env-example .env
- sudo chmod 777 .env -R
- php artisan key:generate
- php artisan db:create {dbName} (After creating database make sure that it matches DB_DATABASE in env file)

- php artisan migrate
- php artisan db:seed --class=ExtensionTableSeeder
- sudo chmod 777 badin-test-project/storage/ -R
- php artisan config:cache

Notice that there isn't any styling in blade files