<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExtensionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('allowed_extensions')->delete();

        DB::table('allowed_extensions')->insert(array (
            0 => [
                'extension' => 'csv'
            ],
            1 => [
                'extension' => 'xml'
            ],
            2 => [
                'extension' => 'xlsx'
            ],
            3 => [
                'extension' => 'tsv'
            ],
            4 => [
                'extension' => 'ods'
            ],
            5 => [
                'extension' => 'slk'
            ],
            6 => [
                'extension' => 'gnumeric'
            ],
            7 => [
                'extension' => 'html'
            ],
            8 => [
                'extension' => 'xls'
            ],
        ));
    }
}
