@extends('layouts.app')

@section('content')

    <form action="{{ url('upload') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-control">
            <label for="importFile">Upload File Here</label>
            <input type="file" name="file" id="importFile" required>
        </div>
        <div class="form-control">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

{{--    @if(session()->has('message'))--}}
{{--        <div class="alert alert-success">--}}
{{--            {{ session()->get('message') }}--}}
{{--        </div>--}}
{{--    @endif--}}
@endsection