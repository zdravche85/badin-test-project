<?php

namespace App\Http\Controllers;

use App\People;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index() : view
    {
        // Load Home View
        return view('home')->with('message', 'Data Imported Successfully');
    }
}
