<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadFileRequest;
use App\Repositories\Format\Interfaces\FormatInterface;
use App\Repositories\Upload\Interfaces\UploadInterface;
use App\Repositories\Validator\Interfaces\ValidationInterface;
use Illuminate\Http\JsonResponse;

class UploadController extends Controller
{
    protected $upload;
    protected $format;
    protected $validator;

    public function __construct(UploadInterface $upload, FormatInterface $format, ValidationInterface $validator)
    {
        $this->upload = $upload;
        $this->format = $format;
        $this->validator = $validator;
    }

    /**
     * @param UploadFileRequest $request
     * @return JsonResponse
     */
    public function uploadFile(UploadFileRequest $request) : JsonResponse
    {
        try{
            // Check for if file extension is allowed
            $this->validator->checkExtension($request->file('file'));
            // Import data to database
            $this->upload->importFile($request->file('file'));
            // Get data for response and properly format it
            $response = $this->format->toJson();
            // Success response
            return response()->json([ 'message' => 'Success', 'data' => $response ], 200);
        }catch (\Exception $e) {
            // Error response
            return response()->json(['message' => $e->getMessage(), 'data' => null ], 400);
        }
    }
}
