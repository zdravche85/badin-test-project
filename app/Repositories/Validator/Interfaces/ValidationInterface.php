<?php

namespace App\Repositories\Validator\Interfaces;

interface ValidationInterface
{
    public function checkExtension($file);
}