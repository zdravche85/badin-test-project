<?php

namespace App\Repositories\Validator;

use App\Repositories\AllowedExtensions\Interfaces\AllowedExtensionsInterface;
use App\Repositories\Validator\Interfaces\ValidationInterface;

class Validator implements ValidationInterface
{
    protected $extension;

    /**
     * Validator constructor.
     * @param AllowedExtensionsInterface $extension
     */
    public function __construct(AllowedExtensionsInterface $extension)
    {
        $this->extension = $extension;
    }

    /**
     * @param $file
     * @throws \Exception
     */
    public function checkExtension($file)
    {
        // Check if extension is allowed, fetch extensions from database
        $extensionExists = $this->extension->getWhere('extension', $file->getClientOriginalExtension());
        // Throw an exception if extension is not allowed
        if(!$extensionExists) {
            throw new \Exception('File Type is Not Allowed');
        }
    }
}