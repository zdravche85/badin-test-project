<?php

namespace App\Repositories\Validator\Providers;

use App\Providers\AppServiceProvider;

class ValidatorServiceProvider extends AppServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Validator\Interfaces\ValidationInterface',
            'App\Repositories\Validator\Validator'
        );
    }
}