<?php

namespace App\Repositories\Upload\Interfaces;

interface UploadInterface
{
    /**
     * @param $file
     * @return mixed
     */
    public function importFile($file);
}