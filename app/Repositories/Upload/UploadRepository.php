<?php

namespace App\Repositories\Upload;

use App\Imports\FileImport;
use App\Repositories\Upload\Interfaces\UploadInterface;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class UploadRepository implements UploadInterface
{
    /**
     * @param $file
     * @return mixed|void
     */
    public function importFile($file)
    {
        // Import file to excel, Excel facade imports it base on extension of a file
        // Supported extensions are 'xml,csv,xls,xlsx,tsv,ods,slk,gnumeric,html' and they are added to validator
        return Excel::import(new FileImport(), $file);
    }
}