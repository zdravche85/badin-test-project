<?php

namespace App\Repositories\Upload\Providers;

use App\Providers\AppServiceProvider;

class UploadServiceProvider extends AppServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Upload\Interfaces\UploadInterface',
            'App\Repositories\Upload\UploadRepository'
        );
    }
}