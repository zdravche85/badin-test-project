<?php

namespace App\Repositories\AllowedExtensions\Providers;

use App\Providers\AppServiceProvider;

class ExtensionServiceProvider extends AppServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\AllowedExtensions\Interfaces\AllowedExtensionsInterface',
            'App\Repositories\AllowedExtensions\AllowedExtensionsRepository'
        );
    }
}