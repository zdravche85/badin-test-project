<?php

namespace App\Repositories\AllowedExtensions\Interfaces;

interface AllowedExtensionsInterface
{
    public function getWhere($column, $value);
}