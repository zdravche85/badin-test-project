<?php

namespace App\Repositories\AllowedExtensions;

use App\AllowedExtension;
use App\Repositories\AllowedExtensions\Interfaces\AllowedExtensionsInterface;

class AllowedExtensionsRepository implements AllowedExtensionsInterface
{
    protected $extension;

    public function __construct(AllowedExtension  $extension)
    {
        $this->extension = $extension;
    }

    public function getWhere($column, $value)
    {
        return $this->extension->where($column, $value)->first();
    }
}