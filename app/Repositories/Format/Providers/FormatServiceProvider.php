<?php

namespace App\Repositories\Format\Providers;

use App\Providers\AppServiceProvider;

class FormatServiceProvider extends AppServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Format\Interfaces\FormatInterface',
            'App\Repositories\Format\FormatRepository'
        );
    }
}