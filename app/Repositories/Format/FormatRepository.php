<?php

namespace App\Repositories\Format;

use App\Repositories\Format\Interfaces\FormatInterface;
use App\Repositories\People\Interfaces\PeopleInterface;

class FormatRepository implements FormatInterface
{
    protected $people;

    /**
     * FormatRepository constructor.
     * @param PeopleInterface $people
     */
    public function __construct(PeopleInterface $people)
    {
        $this->people = $people;
    }

    public function toJson()
    {
        // Create empty collection
        $response = collect();
        // Fill collection with males and females that are adults
        $response->put('males', $this->people->getMaleAdults());
        $response->put('females', $this->people->getFemaleAdults());
        // Convert collection to array and return it
        return $response->toArray();
    }
}