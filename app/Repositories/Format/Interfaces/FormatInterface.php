<?php

namespace App\Repositories\Format\Interfaces;

interface FormatInterface
{
    /**
     * @return mixed
     */
    public function toJson();
}