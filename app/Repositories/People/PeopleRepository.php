<?php

namespace App\Repositories\People;

use App\People;
use App\Repositories\People\Interfaces\PeopleInterface;
use phpDocumentor\Reflection\Types\Collection;

class PeopleRepository implements PeopleInterface
{
    protected $people;

    /**
     * PeopleRepository constructor.
     * @param People $people
     */
    public function __construct(People $people)
    {
        $this->people = $people;
    }

    /**
     * @return mixed
     */
    public function getMaleAdults()
    {
        // Get all male people where age is more than 18 years
        // Function maleAdults() is defined as local scope inside People model
         return $this->people->maleAdults()->get();
    }

    /**
     * @return mixed
     */
    public function getFemaleAdults()
    {
        // Get all female people where age is more than 18 years
        // Function femaleAdults() is defined as local scope inside People model
        return $this->people->femaleAdults()->get();
    }

}