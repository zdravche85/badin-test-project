<?php


namespace App\Repositories\People\Interfaces;

interface PeopleInterface
{
    public function getMaleAdults();

    public function getFemaleAdults();
}