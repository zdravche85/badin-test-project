<?php

namespace App\Repositories\People\Providers;

use App\Providers\AppServiceProvider;

class PeopleServiceProvider extends AppServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\People\Interfaces\PeopleInterface',
            'App\Repositories\People\PeopleRepository'
        );
    }
}