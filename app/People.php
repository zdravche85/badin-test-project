<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class People extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'infix',
        'last_name',
        'date_of_birth',
        'gender',
        'zipcode',
        'house_number'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'first_name' => 'string',
        'infix'  => 'string',
        'last_name'  => 'string',
        'date_of_birth'  => 'date',
        'gender'  => 'enum',
        'zipcode'  => 'string',
        'house_number'  => 'integer'
    ];

    /**
     * @return BelongsTo
     */
    public function file()
    {
        return $this->belongsTo(File::class, 'file_id', 'id');
    }

    public function scopeMaleAdults($query)
    {
        return $query->where('gender', 'm')->where('date_of_birth', '<', Carbon::now()->subYears(18));
    }

    public function scopeFemaleAdults($query)
    {
        return $query->where('gender', 'f')->where('date_of_birth', '<', Carbon::now()->subYears(18));
    }

}
