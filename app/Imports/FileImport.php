<?php

namespace App\Imports;

use App\People;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FileImport implements ToModel, WithBatchInserts, WithChunkReading, WithHeadingRow, WithCustomCsvSettings
{

    public function model(array $row)
    {
        if (!isset($row['firstname']) || !isset($row['lastname'])) {
            return null;
        }

        if($row) {
            return new People([
                'first_name' => $row['firstname'],
                'infix' => $row['infix'],
                'last_name' => $row['lastname'],
                'date_of_birth' => Carbon::parse($row['date_of_birth'])->toDateString(),
                'gender' => $row['gender'],
                'zip_code' => $row['zipcode'],
                'house_number' => $row['housenumber'],
            ]);
        }
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ','
        ];
    }
}
